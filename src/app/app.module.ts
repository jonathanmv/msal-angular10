import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MsalGuardConfiguration, MsalModule, MsalService, MSAL_GUARD_CONFIG, MSAL_INSTANCE } from '@azure/msal-angular';
import { InteractionType, IPublicClientApplication, PublicClientApplication } from '@azure/msal-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PublicPageComponent } from './public-page/public-page.component';
import { RestrictedPageComponent } from './restricted-page/restricted-page.component';

export function MSALInstanceFactory(): IPublicClientApplication {
  return new PublicClientApplication({
    auth: {
      clientId: 'ccbf8484-18b2-417c-8871-2a71e548a9a5', /*From Microsoft Azure APP*/
      redirectUri: 'http://localhost:3000/redirect', /*Must be setting up in Azure app administrator as SPA*/
      authority: 'https://login.microsoftonline.com/ed38846e-062f-4ded-854b-32c9870c1e81' /*API Endpoint with tenant id*/
    }

  });
}

export function MSALGuardConfigFactory(): MsalGuardConfiguration{
  return {
    interactionType: InteractionType.Redirect,

    authRequest: {
      scopes: [
        'code',
        'id_token',
        'code id_token',
        'id_token token'
      ],
      state: '12345',
      nonce: '678910'
    }

  } as MsalGuardConfiguration
}

@NgModule({
  declarations: [
    AppComponent,
    PublicPageComponent,
    RestrictedPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MsalModule
  ],
  providers: [
    {
      provide: MSAL_INSTANCE,
      useFactory: MSALInstanceFactory
    },
    {
      provide: MSAL_GUARD_CONFIG,
      useFactory: MSALGuardConfigFactory
    },
    MsalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
